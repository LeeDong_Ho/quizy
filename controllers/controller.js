module.exports = function Contr ( model )
{
    this.model = model;
    this.mongoose = require('mongoose');

    this.handleError = function (err, res) {
        if(err.code) return this.handleErrorCode(err, res);

        switch (err.name) {
            case 'ValidationError' :
                return this.handleValidError(err, res);;
            case 'CastError' :
                return res.status(400).send(err.message);
            default :
                if(err.message) return this.handleErrorMsg(err, res);
                res.status(500).send();
        }
    };

    this.handleErrorMsg = function(err, res) {
       switch (err.message) {
           case 'Unauthorized' :
               return res.status(401).send();
           default :
               res.status(500).send();
       }
    }

    this.handleErrorCode = function (err, res) {
        switch (err.code) {
            case 401 :
                return res.status(401).send();
            case 404 :
                return res.status(404).send();
            default :
                res.status(500).send();
        }
    }

    this.handleValidError = function (err, res) {
        if(err.errors.user_id) res.status(401).send();
        else
            res.status(500).send();
    };
}