/**
 * Created by leedong-ho on 2014. 6. 9..
 */
var util = require('util');
var Contr = require('./controller');
var model = require('../models/student').getInstance();
var instance;
var StudentContr = function StudentContr( model ) {
    StudentContr.super_.call(this, model);

    this.markMc = function(mc, mcRes) {
        if(!mcRes.cb || mc.cb.length != mcRes.cb.length) {
            mcRes.correct_cb = mc.cb;
            return;
        }

        for(var i = 0;i < mcRes.cb.length; ++i) {
            if(mc.cb[i] != mcRes.cb[i]) {
                mcRes.correct_cb = mc.cb;
                return;
            }
        }
    }

    this.markSa = function(sa, saRes) {
        if(sa.ans.trim().toLowerCase() != saRes.ans.trim().toLowerCase()) saRes.correct_ans = sa.ans;
    }

    this.markQuizRes = function(quiz, quizRes) {
        var quiz = quiz.quiz,
            quizRes = quizRes.quiz;

        for(var i = 0;i < quizRes.length;++i) {
            var qz = quiz[i],
                qzRes = quizRes[i];

            if(qz.type == 'mc') this.markMc(qz, qzRes);
            else this.markSa(qz, qzRes);
        }
    }

    this.saveQuizRes = function(req, res) {
        var quizId = req.params.quiz_id,
            quizRes = req.body,
            quizResNewDoc = this.model.getNewDoc(),
            quizModel = require('../models/quiz').getInstance(),
            async = require('async'),
            context = this;

        if(!quizId) {
            var err = new Error('No id');
            err.code = 404;
            return this.handleError(err, res);
        }

            try {
                async.waterfall([
                    function getOriginalQuiz(callback) {
                        var quizObjId = new quizModel.mongoose.Types.ObjectId(quizId);
                        quizModel.coll.findOne({_id:quizObjId}, function(err, quiz){
                            callback(err, quiz);
                        });
                    },
                    function markQuizRes(quiz, callback) {
                        if(!quiz) {
                            var err = new Error('No quiz');
                            err.code = 404;
                            return context.handleError(err,res);
                        }

                        context.markQuizRes(quiz, quizRes);

                        quizResNewDoc.quiz_id = quiz._id;
                        quizResNewDoc.quiz_res = quizRes;
                        quizResNewDoc.save(function(err){
                            callback(err, quiz);
                        });
                    },
                    function returnSuccess(quiz){
                        if(!req.session.quiz) req.session.quiz = {};

                        req.session.quiz[quiz._id.toString()] = true;
                        res.status(204).set('Location','/student').send();
                    }
                ],
                function (err) {
                    if(err) context.handleError(err,res);
                });
            } catch(err) {
                this.handleError(err, res);
            }
    };
};
util.inherits(StudentContr,Contr);
exports.getInstance = function() {
    if(!instance) instance = new StudentContr(model);
    return instance;
};