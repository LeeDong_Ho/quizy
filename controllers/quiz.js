var util = require('util');
var Contr = require('./controller');
var model = require('../models/quiz').getInstance();
var instance;
var QuizContr = function quizContr( model ) {
    QuizContr.super_.call(this, model);
    this.create = function (req, res) {
        var newQuizDoc,
            context = this,
            async = require('async');

        newQuizDoc = this.model.getNewDoc();
        newQuizDoc.user_id = req.session.user ? req.session.user._id : null;
        newQuizDoc.quiz = req.body.quiz;
        newQuizDoc.quiz_sbjt = req.body.quiz_sbjt;
        newQuizDoc.save(function(err) {
            if(err) return context.handleError(err, res);
            res.status(204).set('Location','/user').send();
        });
    };

    this.saveUploadedFile = function (req, res) {
        var multiparty = require('multiparty'),
            form = new multiparty.Form(),
            async = require('async'),
            conf = require('../conf'),
            context = this;

            try {
                async.waterfall([
                    function parseBody(callback) {
                        form.parse(req, function parsedBody(err, field, files) {
                            callback(err, field, files);
                        });
                    },
                    function isUploaded(field, files, callback) {
                        var file = (files.f && files.f.length > 0) ? files.f.pop() : null;

                        if(!file) {
                            var err = new Error('No uploaded file!');
                            err.code = 500;
                            return callback(err);
                        }

                        callback(null, file);
                    },
                    function saveFile(file, callback) {
                        var user = req.session.user,
                            Grid = require('gridfs-stream'),
                            conn = context.model.uploadingConn,
                            mongoose = context.mongoose;

                        if (!user) {
                            var err = new Error();
                            err.code = 401;
                            return callback(err);
                        }

                        var gfs = Grid(conn.db, mongoose.mongo),
                            newObjId = new mongoose.Types.ObjectId(),
                            option = {
                                _id : newObjId.toString(),
                                filename : file.originalFilename,
                                size : file.size,
                                metadata : {
                                    user_id : user._id.toString(),
                                    contentType : file.headers['content-type']
                                }
                            },
                            writeStream = gfs.createWriteStream(option),
                            fs = require('fs');

                        fs.createReadStream(file.path).pipe(writeStream);

                        writeStream.on('close', function(file){
                            callback(null, file, newObjId);
                        });
                    },
                    function closedFile(file, newObjId) {
                        res.status(201).set('Location','/quiz/uploaded/' + newObjId.toString()).json({contentType:file.metadata.contentType, fileName:file.filename});
                    }
                ],
                function(err, result) {
                   if(err) context.handleError(err, res);
                });
            } catch (err) {
                this.handleError(err, res);
            };
    };

    this.readUploadedFile = function(req, res) {
        var async = require('async'),
            context = this,
            conf = require('../conf');

        try {
            async.waterfall([
                function readMetaData (callback) {
                    var conn = context.model.uploadingConn,
                        mongoose = context.mongoose;

                    if(!req.params || !req.params.id) {
                        var err = new Error('no file id');
                        err.code = 404;
                        callback(err);
                    }

                    var Grid = require('gridfs-stream'),
                        gfs = Grid(conn.db, mongoose.mongo),
                        objId = new mongoose.Types.ObjectId(req.params.id);

                    gfs.files.findOne({ _id : objId }, function(err, file) {
                        callback(err, file, gfs, mongoose, objId);
                    });
                },
                function readFile(file, gfs, mongoose, objId, callback) {

                    if(!file)
                    {
                        var err = new Error();
                        err.code = 404;
                        return callback(err);
                    }

                    var fs = require('fs'),
                        ext = file.metadata.contentType.split('/')[1],
                        filePath = 'temp/'+ objId.toString() + '.' + ext;

                    fs.exists(filePath, function(exist) {
                        var readStream,
                            writeStream;

                        if(!exist) {
                            readStream = gfs.createReadStream({
                                _id : objId
                            });

                            writeStream = fs.createWriteStream(filePath);

                            readStream.pipe(writeStream);

                            writeStream.once('close', function(){
                                callback(null,file, filePath);
                            })

                            writeStream.once('error',function (err) {
                                err.code = 500;
                                callback(err);
                            })

                            readStream.once('error', function (err) {
                                if(err.message && err.message.search('does not exist') != -1) err.code = 404;
                                callback(err);
                            });
                        } else {
                            callback(null, file, filePath);
                        }
                    });
                },
                function sendFile(file, path) {
                    res.sendfile(path);
                }
            ],
            function (err, result) {
                if(err) return context.handleError(err, res);
            });
        } catch (err) {
            if(err) this.handleError(err, res);
        }
    };

    this.removeUploadData = function (req, res)
    {
        var async = require('async'),
            conf = require('../conf'),
            context = this;

        try {
            async.waterfall([
                function openConn(callback) {
                    var mongoose = context.mongoose,
                        conn = mongoose.createConnection(conf.uploading.url);

                    conn.once('open', function() {
                        callback(null, mongoose, conn);
                    });
                },
                function readMetaData(mongoose, conn, callback) {
                    var Grid = require('gridfs-stream'),
                        gfs = Grid(conn.db, mongoose.mongo),
                        objId;

                   if(!req.params.id) {
                       var err = new Error();
                       err.code = 404;
                       return callback(err);
                   }

                   objId = new mongoose.Types.ObjectId(req.params.id);

                   gfs.files.findOne({ _id : objId },function(err, file) {
                        if(!err && !file) return res.status(404).send();
                        callback(err, file, gfs, objId);
                   });
                },
                function removeFile(file, gfs, objId, callback) {
                    var user = req.session.user,
                        ext = file.metadata.contentType.split('/')[1],
                        path = 'temp/' + objId.toString() + '.' + ext,
                        fs = require('fs');

                    if(!user || user._id != file.metadata.user_id) return callback(Error('Unauthorized'));

                    fs.exists(path, function(exist){
                        if(exist) fs.unlink(path, function(err) {
                            console.log(err);
                        });
                    });

                    gfs.remove({_id:file._id}, function(err){
                       callback(err);
                    });
                },
                function removed() {
                    res.status(204).send();
                }
            ], function (err) {
                context.handleError(err, res);
            });
        } catch (err) {

        }
    };

    this.startQuiz = function(req, res) {
        var quizModel = require('../models/quiz').getInstance(),
            async = require('async'),
            context = this,
            quizObjId;

        if(!req.params || !req.params.id) res.status(404).send();

        quizObjId = new quizModel.mongoose.Types.ObjectId(req.params.id);

        try {
            async.waterfall([
                function findQuiz(callback) {
                    quizModel.getQuiz(quizObjId, function(err, quiz) {
                       callback(err, quiz);
                    });
                },
                function updateStartFlag(quiz) {
                    quiz.start = true;
                    quiz.save();
                    res.status(204).send();
                }
            ],
            function(err) {
                context.handleError(err);
            });
        } catch(err) {
            this.handleError(err);
        }
    };

    this.endQuiz = function (req, res) {
        var quizModel = require('../models/quiz').getInstance(),
            async = require('async'),
            context = this,
            quizObjId;

        if(!req.params || !req.params.id) res.status(404).send();

        quizObjId = new quizModel.mongoose.Types.ObjectId(req.params.id);

        try {
            async.waterfall([
                    function findQuiz(callback) {
                        quizModel.getQuiz(quizObjId, function(err, quiz) {
                            callback(err, quiz);
                        });
                    },
                    function updateStartFlag(quiz) {
                        quiz.start = false;
                        quiz.save();
                        res.status(204).send();
                    }
                ],
                function(err) {
                    context.handleError(err);
                });
        } catch(err) {
            this.handleError(err);
        }
    };

    this.getStartedQuiz = function(roomIdx, callback) {
        if(!roomIdx) return callback(new Error('No id'));

        var async = require('async'),
            userModel = require('../models/user').getInstance(),
            quizModel = require('../models/quiz').getInstance();

        try {
            async.waterfall([
                    function findUserByRoomIdx(callback) {
                        userModel.findUser({room_idx : roomIdx}, function(err, user) {
                           callback(err, user);
                        });
                    },
                    function findQuiz(user, callback) {
                        if(!user) {
                            var err = new Error('No user with matched room_idx');
                            err.code = 404;
                            return callback(err);
                        }
                        quizModel.getQuizOne({user_id : user._id, start : true}, function(err, quiz) {
                            callback(err, quiz);
                        });
                    },
                    function exmineQuizIsStarted(quiz) {
                        callback(null, quiz);
                    }
                ],
                function(err) {
                    callback(err);
                });
        } catch(err) {
            callback(err);
        };
    };

    this.renderListEndedQuiz = function(req, res){
        var user = req.session.user,
            async = require('async'),
            context = this;

        if(!user) {
            var err = new Error('Not login');
            err.code(401);
            return this.handleError(err, res);
        }

        try {
            async.waterfall([
                function findUserQuizzes(callback) {
                    var quizModel = require('../models/quiz').getInstance();

                    quizModel.getQuizzesByQry({user_id:user._id},function(err, quizzes) {
                        callback(err, quizzes);
                    });
                },
                function findUserQuizRes(quizzes,callback) {
                    var studentModel = require('../models/student').getInstance(),
                        arrObjIds = new Array();

                    for(var i = 0;i < quizzes.length;++i) {
                        arrObjIds[i] = quizzes[i]._id;
                    }

                    studentModel.findQuizzes({quiz_id : {
                        $in : arrObjIds
                    }}, function(err, studentQuizzes){
                        callback(err, quizzes, studentQuizzes);
                    });
                },
                function filterQuizzes(quizzes, studentQuizzes, callback){
                    var quizModel = require('../models/quiz').getInstance(),
                        arrObjIds = new Array();

                    if(!studentQuizzes) return callback(null, null);

                    for(var i = 0;i < studentQuizzes.length;++i) {
                        arrObjIds[i] = studentQuizzes[i].quiz_id;
                    }

                    if(arrObjIds.length <= 0) return callback(null, null);

                    quizModel.getQuizzesByQry({
                        _id : {
                            $in : arrObjIds
                        }
                    }, function(err, filteredQuizzes) {
                        callback(err, filteredQuizzes);
                    })
                },
                function renderList(quizzes, callback) {
                    res.render('resQuizList', {quizzes:quizzes});
                }
            ], function(err) {
                if(err) context.handleError(err, res);
            });
        }catch (err) {
            if(err) this.handleError(err, res);
        }
    };

    this.renderQuizResList = function(req, res){
        var quiz_id = req.params.quiz_id,
            async = require('async'),
            context = this;

        if(!quiz_id) {
            var err = new Error('Not quiz id');
            err.code(401);
            return this.handleError(err, res);
        }

        try {
            async.waterfall([
                function findQuiz(callback) {
                    var quizModel = require('../models/quiz').getInstance();

                    quizModel.coll.findById(quiz_id,function(err, quiz){
                        callback(err, quiz);
                    });
                },
                function findStudentsQuizzes(quiz, callback) {
                    var studentModel = require('../models/student').getInstance();

                    studentModel.findQuizzes({quiz_id:quiz_id},function(err, resQuizzes) {
                        callback(err, quiz, resQuizzes);
                    });
                },
                function renderList(quiz, resQuizzes) {
                    res.render('resStudentQuizList', {quiz:quiz, resQuizzes:resQuizzes});
                }
            ], function(err) {
                if(err) context.handleError(err, res);
            });
        }catch (err) {
            if(err) this.handleError(err, res);
        }
    };

    this.renderEndedStudentQuiz = function(req, res) {
        var studentQuizId = req.params.student_quiz_id,
            studentModel = require('../models/student').getInstance(),
            quizModel = require('../models/quiz').getInstance(),
            async = require('async'),
            context = this,
            studentQuizObjId;

        if(!studentQuizId) {
            var err = new Error('No student quiz id');
            err.code = 404;
            return this.handleError(err, res);
        }

        studentQuizObjId = new studentModel.mongoose.Types.ObjectId(studentQuizId);

        try {
            async.waterfall([
                function findStudentQuiz(callback) {
                   studentModel.coll.findOne({_id:studentQuizObjId}, function(err, quizRes){
                        callback(err, quizRes);
                   });
                },
                function findOriginalQuiz(quizRes, callback) {
                    if(!quizRes) {
                        var err = new Error('No student quiz');
                        err.code = 404;
                        return context.handleError(err, res);
                    }
                    quizModel.getQuizOne({_id:quizRes.quiz_id},function(err, quiz) {
                        callback(err, quizRes, quiz);
                    });
                },
                function renderMarkedStudentQuiz(quizRes, quiz) {
                    res.render('studentQuizRes', {quizRes : quizRes, quiz : quiz});
                }
            ], function(err){
                if(err) context.handleError(err);
            })
        }catch(err){
            if(err) this.handleError(err);
        }
    };

    this.updateUploadedFileFromDoc = function (req, res) {
        if(!req.body.quiz_id || !req.body.url) {
            var err = new Error('No quiz_id, url');
            err.code = 404;
            this.handleError(err);
        }

        var quizModel = require('../models/quiz').getInstance(),
            quizObjId = quizModel.mongoose.Types.ObjectId(req.body.quiz_id),
            async = require('async'),
            context = this;
        try {
            async.waterfall([
                function findQuiz(callback) {
                    quizModel.coll.findById(req.body.quiz_id, function (err, quiz) {
                        callback(err, quiz);
                    });
                },
                function updateDoc(quiz, callback) {
                    if (!quiz) {
                        var err = new Error('No quiz');
                        err.code = 404;
                        context.handleError(err, res);
                        return;
                    }

                    var qz = quiz.quiz,
                        qzLen = qz.length,
                        url = req.body.url;

                    for (var i = 0; i < qzLen; ++i) {
                        var item = qz[i];

                        if (item.upload && item.upload.url == url) {
                            item.upload.url = '';
                            item.upload.content_type = '';
                            quiz.update({$set: {quiz: quiz.quiz}}, function (err) {
                                callback(err);
                            });
                            return;
                        }
                    }

                    var err = new Error('Can\'t find this url is \"' + url + '"');
                    err.code = 404;
                    context.handleError(err, res);
                },
                function response() {
                    res.status(204).send();
                }
            ], function (err) {
                if (err) context.handleError(err, res);
            });
        } catch (err) {
            this.handleError(err, res);
        }
    };

    this.update = function(req, res) {
        var quizModel = require('../models/quiz').getInstance(),
            async = require('async'),
            context = this,
            id = req.body.quiz_id;

        try {
            async.waterfall([
                function findQuiz (callback) {
                    quizModel.coll.findById(id, function(err, quiz) {
                        callback(err, quiz);
                    });
                },
                function updateQuiz(quiz, callback) {
                    if(!quiz) {
                        var err = new Error('No quiz');
                        err.code = 404;
                        context.handleError(err, res);
                        return;
                    }

                    var mdfdQzSet = req.body;

                    quiz.update({
                        $set : { quiz_sbjt : mdfdQzSet.quiz_sbjt, quiz : mdfdQzSet.quiz, mod_date : new Date() }
                    }, function(err, numberAffected, raw) {
                        callback(err, numberAffected, raw);
                    });
                },
                function response(numberAffected, raw) {
                    res.status(204).set('Location','/user').send();
                }
            ],
            function hadleError(err) {
                context.handleError(err);
            });
        } catch (err) {
            this.handleError(err, res);
        }

    };

    this.remove = function(req, res) {
        if(!req.params.id) {
            var err = new Error('no quiz id');
            err.code = 404;
            this.handleError(err, res);
            return;
        }

        var quizModel = require('../models/quiz').getInstance(),
            async = require('async'),
            context = this,
            user = req.session.user;

        try {
            async.waterfall([
                function findQuiz(callback) {
                    quizModel.coll.findById(req.params.id, function(err, quiz) {
                        callback(err, quiz);
                    });
                },
                function authAndRemoveQuizRes(quiz, callback) {
                    if(!quiz) {
                        var err = new Error('No quiz');
                        err.code = 404;
                        context.handleError(err, res);
                        return;
                    }

                    if(quiz.user_id != user._id) {
                        var err = new Error('Not user\'s quiz');
                        err.code = 401;
                        context.handleError(err, res);
                        return;
                    }

                    var studentModel = require('../models/student').getInstance();

                    studentModel.coll.find({quiz_id:quiz._id}).remove().exec(function(err){
                        callback(err, quiz);
                    });
                },
                function removeQuiz(quiz, callback) {
                    quiz.remove(function(err){
                        callback(err);
                    });
                },
                function response(){
                    res.status(204).send();
                }
            ], function handleErr(err) {
                if(err) context.handleError(err, res);
            });
        } catch (err) {
            this.handleError(err, res);
        }

    };

    this.makeXlsxAllRes = function (quiz, quizAllRes) {
        var xlsx = require('node-xlsx'),
            fs = require('fs'),
            sheet = {
                "name":quiz.quiz_sbjt,
                "data":[]
            },
            header = [],
            xlsxObj = {worksheets: []};


        header.push('name');
        for(var i = 0;i<quiz.quiz.length;++i) {
            header.push(quiz.quiz[i].ques);
        }
        header.push('Sum O');
        header.push('Sum X');

        sheet.data.push(header);

        for(var i = 0;i<quizAllRes.length;++i) {
            var qzRes = quizAllRes[i],
                hco = 0,
                hcx = 0,
                row = [];

            row.push(qzRes.quiz_res.name);

            for(var j = 0;j < qzRes.quiz_res.quiz.length;++j) {
                var qzItem = qzRes.quiz_res.quiz[j];


                if(qzRes.quiz_res.type[j] == 'mc') {
                    if(qzItem.correct_cb) {
                        row.push({value:'X',formatCode:'General'});
                        ++hcx;
                    }
                    else {
                        row.push({value:'O',formatCode:'General'});
                        ++hco;
                    }
                }
                else {
                    if(qzItem.correct_ans) {
                        row.push({value:'X',formatCode:'General'});
                        ++hcx;
                    }
                    else {
                        row.push({value:'O',formatCode:'General'});
                        ++hco;
                    }
                }
            }

            row.push(hco);
            row.push(hcx);
            sheet.data.push(row);
        }

        var rowVco = [],
            rowVcx = [];

        rowVco.push({value:'Sum O',formatCode:'General'});
        rowVcx.push({value:'Sum X',formatCode:'General'});

        for (var i = 1; i <= quiz.quiz.length; ++i) {
            var vco = 0,
                vcx = 0;

            for (var j = 1; j < sheet.data.length; ++j) {
                if(sheet.data[j][i].value == 'O') ++vco;
                else ++vcx;
            }

            rowVco.push({value:vco, formatCode:'General'});
            rowVcx.push({value:vcx, formatCode:'General'});
        }

        sheet.data.push(rowVco);
        sheet.data.push(rowVcx);

        xlsxObj.worksheets.push(sheet);
        return xlsx.build(xlsxObj);
    };

    this.downloadXlsxAllRes = function (res, quiz, quizAllRes) {
        var buffer = this.makeXlsxAllRes(quiz, quizAllRes);
        res.set('Content-Type','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.send(buffer);
    };

    this.reportOfAllQuizRes = function(req, res) {
        var quizModel = require('../models/quiz').getInstance(),
            async = require('async'),
            user = req.session.user,
            quiz_id = req.params.id,
            isXlsx = req.query.xlsx,
            isEmail = req.query.email,
            context = this;

        if(!quiz_id) {
            var err = new Error('no quiz id');
            err.code = 404;
            return this.handleError(err, res);
        }

        try {
            async.waterfall([
                function findQuiz (callback) {
                    quizModel.coll.findById(quiz_id, function(err, quiz) {
                        callback(err, quiz);
                    });
                },
                function findQuizAllRes (quiz, callback) {
                    if(!quiz) {
                        var err = new Error('no quiz');
                        err.code = 404;
                        return callback(err);
                    }

                    if(user._id != quiz.user_id) {
                        var err = new Error('Not user\'s quiz');
                        err.code = 401;
                        return callback(err);
                    }

                    var studentModel = require('../models/student').getInstance();

                    studentModel.coll.find({quiz_id:quiz._id}, function(err, quizAllRes) {
                        callback(err, quiz, quizAllRes);
                    });
                },
                function report (quiz, quizAllRes) {
                    if(isEmail){
                        var mailOpts = {},
                            attachment = {},
                            xlsx;

                        mailOpts.to = user.email;
                        mailOpts.subject = 'Quizy✔' + quiz.quiz_sbjt + ' report!';

                        if(isXlsx) {
                            xlsx = context.makeXlsxAllRes(quiz, quizAllRes);
                            mailOpts.attachments = [];
                            attachment.fileName = quiz.quiz_sbjt;
                            attachment.contents = xlsx;
                            attachment.contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
                            mailOpts.attachments.push(attachment);
                        } else {
                            mailOpts.html = context.makeHtmlAllRes(quiz, quizAllRes);
                        }

                        context.sendReportEmail(res, mailOpts);
                    }else {
                        if(isXlsx) context.downloadXlsxAllRes(res, quiz, quizAllRes);
                        else res.render('reportAllQuizRes',{quiz:quiz, quizAllRes : quizAllRes});
                    }
                }
            ], function(err) {
                context.handleError(err, res);
            });
        } catch (err) {
            if(err) this.handleError(err, res);
        }
    };

    this.makeHtmlAllRes = function (quiz, quizAllRes) {
        var jade = require('jade');

        return jade.renderFile('views/reportAllQuizRes.jade', {quiz:quiz, quizAllRes : quizAllRes});
    }

    this.sendReportEmail = function (res, mailOpts) {

        var nodemailer = require('nodemailer'),
            conf = require('../conf'),
            // create reusable transport method (opens pool of SMTP connections)
            smtpTransport = nodemailer.createTransport("SMTP",{
                service: "Gmail",
                auth: {
                    user: conf.email.gmail.user,
                    pass: conf.email.gmail.pass
            }
        }),
        extend = require('node.extend'),
        // setup e-mail data with unicode symbols
        defaultMailOpts = {
            from: "Quizy✔ <" + conf.email.gmail.user + ">", // sender address
            subject: "Quizy✔ report" // Subject line
        },
        context = this;

        extend(true, defaultMailOpts, mailOpts);

        // send mail with defined transport object
        smtpTransport.sendMail(defaultMailOpts, function(err, response){
            if(err) return context.handleError(err, res);
            res.status(200).send(response.message);
            // if you don't want to use this transport object anymore, uncomment following line
            smtpTransport.close(); // shut down the connection pool, no more messages
        });
    };
};
util.inherits(QuizContr,Contr);
exports.getInstance = function() {
    if(!instance) instance = new QuizContr(model);
    return instance;
};