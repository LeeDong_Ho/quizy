var util = require('util');
var Contr = require('./controller');
var model = require('../models/user').getInstance();
var instance;
var UserContr = function userContr(model) {

    UserContr.super_.call(this, model);

    this.create = function(req,res) {
        var email = req.body.email,
            context = this,
            async = require('async');

        try {
            async.waterfall([
                function findUser(callback) {
                    context.model.coll.findOne({email:email},'email',function(err, user) {
                        callback(err, user);
                    });
                },
                function saveUser(user, callback) {
                    if(user) return res.status(409).send();

                    var doc = context.model.getNewDoc();

                    doc.email = req.body.email;
                    doc.pass = req.body.pass;
                    doc.save(function(err) {
                        callback(err, doc);
                    });
                },
                function updateRoomIdx(doc, callback) {
                    var hashFunc = require('string-hash');

                    doc.room_idx = hashFunc(doc._id.toString());
                    doc.save(function(err) {
                        callback(err, doc);
                    });
                },
                function sendAuthEmail(user, callback) {
                    context.sendAuthMail(user, function(err, response){
                        callback(err, response);
                    });
                },
                function success(response, callback) {
                    res.status(200).set('Location', '/user/login').send(response.message);
                }
            ], function(err, result) {
                context.handleError(err, res);
                console.log(result);
            });
        } catch(err) {
            this.handleError(err, res);
        };
    };

    this.sendAuthMail = function (user, callback) {
        var nodemailer = require('nodemailer'),
            conf = require('../conf'),
            smtpTransport = nodemailer.createTransport("SMTP", {
                service: "Gmail",
                auth: {
                    user: conf.email.gmail.user,
                    pass: conf.email.gmail.pass
                }
            }),
            hmac = this.getAuthHmac(user),
            html,
            url,
            mailOpts = {
                from: "Quizy✔ <" + conf.email.gmail.user + ">", // sender address
                subject: "Quizy✔ email authentication" // Subject line
            };


        url = conf.host.protocol + '://' + conf.host.domain + ':' + conf.host.https_port + '/user/auth/' + user._id.toString() + '/' + hmac;
        html = '<h1>Click link for your authentication Quizy\'s email!</h1>';
        html += '<a href="' + url + '">' + url + '</a>';

        mailOpts.to = user.email;
        mailOpts.html = html;

        // send mail with defined transport object
        smtpTransport.sendMail(mailOpts, function(err, response){
            // if you don't want to use this transport object anymore, uncomment following line
            smtpTransport.close(); // shut down the connection pool, no more messages

            callback(err, response);
        });
    };

    this.getAuthHmac = function(user) {
        var hash = this.getPossibleHash();

        hash.update('hex', user._id.toString(), user.reg_date.getTime());
        return hash.digest('hex');
    };

    this.getPossibleHash = function() {
        var crypto = require('crypto'),
            hashes = crypto.getHashes();
        for(var i=0;i<hashes.length;++i) {
            switch (hashes[i]) {
                case 'sha512':
                    return crypto.createHash('sha512');
                case 'sha384':
                    return crypto.createHash('sha384');
                case 'sha256':
                    return crypto.createHash('sha256');
            }
        }
    }

    this.login = function(req, res) {
        var email = req.body.email,
            pass = req.body.pass,
            async = require('async'),
            context = this;

        try {
            async.waterfall([
                function findUser(callback) {
                    context.model.coll.findOne({email: email, pass: pass}, function (err, user) {
                        callback(err, user);
                    });
                },
                function authUser(user, callback) {
                    if(!user) return res.status(404).send();
                    if(!user.email_auth) return res.status(401).send();

                    req.session.user = user._doc;
                    res.status(204).set('Location','/user').send();
                }
            ], function (err, result) {
                if(err) throw err;
                console.log(result);
            });
        } catch(err) {
            if(err) this.handleError(err);
        }
    };

    this.isExistedId = function(req, res) {
        var email,
            async = require('async'),
            context = this;

        email = req.body.email;

        try {
            async.waterfall([function findUserByEmail (callback) {
                context.model.coll.findOne({email:email}, function(err, user) {
                    callback(err, user);
                });
            },
            function response(user, callback) {
                if(user) res.status(200).send(true);
                else res.status(200).send(false);
            }], function(err, result){
                if (err) throw err;
                console.log(result);
            });
        } catch (err) {
            if(err) res.status(500).send();
        };
    };

    this.isNotExistedId = function(req, res) {
        var email,
            async = require('async'),
            context = this;

        email = req.body.email;

        try {
            async.waterfall([function findUserByEmail(callback) {
                context.model.coll.findOne({email: email}, function (err, user) {
                    callback(err, user);
                });
            },
            function response(user, callback) {
                if(user) res.status(200).send(false);
                else res.status(200).send(true);
            }], function(err){
                if(err) throw err;
                console.log(err);
            });
        } catch (err) {
            if(err) res.status(500).send();
        };
    };

    this.isExistedRoomIdx = function(req, res) {
        var room_idx,
            async = require('async'),
            context = this;

        room_idx = req.body.room_idx;

        try {
            async.waterfall([function findUserByRoomIdx(callback) {
                context.model.coll.findOne({room_idx: room_idx}, function (err, user) {
                    callback(err, user);
                });
            },
                function response(user, callback) {
                    if(user) res.status(200).send(true);
                    else res.status(200).send(false);
                }], function(err){
                if(err) throw err;
                console.log(err);
            });
        } catch (err) {
            if(err) res.status(500).send();
        };
    };

    this.isLoggedIn = function (req) {
        if(req.session.user) return true;
        return false;
    };

    this.mustLogin = function(req, res) {
        if(!this.isLoggedIn(req)) {
            res.status(301).set('Location', '/user/login').send();
            return true;
        }

        return false;
    };

    this.authEmail = function(req, res) {
        var userModel = require('../models/user').getInstance(),
            async = require('async'),
            receivedId = req.params.id,
            receivedhmac = req.params.hmac,
            context = this;

        try {
            async.waterfall([
                    function findUser(callback) {
                        userModel.coll.findById(receivedId, function(err, user) {
                            callback(err, user);
                        });
                    },
                    function compareHmac(user, callback) {
                        var hmac = context.getAuthHmac(user);

                        if(receivedhmac != hmac) {
                            var err = new Error('malformed auth param');
                            err.code = 401;
                            return callback(err);
                        }

                        user.email_auth = true;
                        user.update({$set:{email_auth:true}}, function(err) {
                            callback(err);
                        });
                    },
                    function response() {
                        res.render('authedMail', {authed:true});
                    }
                ],
                function (err) {
                    res.render('authedMail', {authed:false});
            });
        } catch (err) {
            res.render('authedMail', {authed:false});
        }
    };

    this.logout = function(req, res) {
        if(req.session.user) delete req.session.user;
        res.status(204).send();
    };
};
util.inherits(UserContr, Contr);
exports.getInstance = function() {
    if(!instance) instance = new UserContr(model);
    return instance;
};