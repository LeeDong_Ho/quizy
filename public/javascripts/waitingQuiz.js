/**
 * Created by leedong-ho on 2014. 6. 9..
 */
$('#waiting-page').on('pagebeforeshow',function() {
    var $waitingForm = $('#waiting-form'),
        timer;

    function setReqFlag (bool) {
        $waitingForm.data('isRequesting', bool);
    }
    function isReq() {
        return $waitingForm.data('isRequesting');
    }

    setReqFlag(false);
    $waitingForm.ajaxForm({
        type : 'head',
        success : function(response, statusText, jqxhr, $form) {
            $.mobile.navigate(jqxhr.getResponseHeader('Location'));
            clearInterval(timer);
            $.mobile.loading('hide');
            setReqFlag(false);
        },
        error : function(jqxhr, statusText, errorThrown, $form) {
            setReqFlag(false);
            if(jqxhr.status != 404) {
                clearInterval(timer);
                //오류 메세지 처리
            }
        }
    });

    timer = setInterval(function(){
        if(!isReq()) {
            $waitingForm.submit();
            setReqFlag(true);
        }
    },5000);
});
$('#waiting-page').on('pageshow',function() {
    $.mobile.loading('show', {
        text: "Waiting started Quiz",
        textVisible: true
    });
});
