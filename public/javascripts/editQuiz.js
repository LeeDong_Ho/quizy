/**
 * Created by leedong-ho on 2014. 6. 23..
 */
function addMcExistQues(cb)
{
    var $cb = $(cb),
        $cbWrap = $cb.closest('.ui-checkbox'),
        $label = $cb.parent().find('label'),
        $hidden = $cbWrap.next('input[type=hidden]');

    $cbWrap.append($hidden);
    $cb.click({label:$label, cb:$cb, hidden:$hidden}, popupToEditMcQues);

    $cb.rules( 'add', {
        required:true,
        minlength:1,
        messages : {
            required : 'you must choose one or more'
        }
    });
}
function removeUploadedFileFromQuiz(e) {
    var $btn = $(e.target),
        $uploadedDatawrap = $btn.parent('div'),
        $innerWrap = $uploadedDatawrap.find('.inner-wrap'),
        $media = $innerWrap.find('img, video, audio'),
        qzId = $btn.closest('form').find('input[type=hidden][name=quiz_id]').val(),
        url;

    if($media.is('img')) url = $media.attr('src');
    else if ($media.is('video') || $media.is('audio')) url = $media.find('source').attr('src');

    if(!url) return;

    $.ajax({
        url : '/quiz/uploaded',
        data : {url:url,quiz_id:qzId},
        type : 'put',
        error : function (jqxhr) {
            handleAjaxError(jqxhr);
        }
    });
}
$('#edit-quiz-page').on('pagecreate', function(){
    var $form = $('#edit-quiz-page').find('form#quiz-form'),
        $cbs = $form.find('.mc-cb:checkbox'),
        $mcs = $form.find('.qz-mc'),
        $sas = $form.find('.qz-sa'),
        $video = $form.find('video'),
        $removeFileBtn = $form.find('button.remove-file-btn'),
        cbsLength = $cbs.length;

    $removeFileBtn.one('click', removeUploadedFileFromQuiz);

    if(addMc) addMc.count = $mcs.length;
    if(addSa) addSa.count = $sas.length;

    for(var i = 0;i < cbsLength;++i) {
        addMcExistQues($cbs.eq(i));
    }
});