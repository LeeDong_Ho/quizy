/**
 * Created by leedong-ho on 2014. 6. 9..
 */
$('#solving-quiz-page').on('pagecreate',function(){
    var $form = $('#solve-quiz-form');

    var validOpts = {
        errorPlacement : function(err, elem) {
            if(elem.is(':checkbox')){
                elem.closest('[data-role=controlgroup]').find('.error-placement').append(err);
            } else {
                elem.after(err);
            }
        },
        submitHandler : function(form) {
            var $form = $(form);

            if(!$form.valid()) return;

            $form.ajaxSubmit({
                beforeSubmit : function() {
                    $.mobile.loading('show');
                },
                success:function(response, statusText, jqxhr) {
                    var loc = jqxhr.getResponseHeader('Location');

                    if(loc) $.mobile.navigate(loc + '/?' + $.now());
                },
                error:function(jqxhr, statusText, errorThrown) {
                    //error handle
                },
                complete : function() {
                    $.mobile.loading('hide');
                }
            });
        }
    };

    $form.validate(validOpts);

    $form.find('video').click(function(e){
        playVideo(e.target);
    });
});