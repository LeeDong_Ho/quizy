/**
 * Created by leedong-ho on 2014. 6. 29..
 */
$('#auth-email-page').on('pagecreate', function(){
    var $form = $('#auth-email-form');

    $form.validate({
        rules : {
            email: {
                required: true,
                email: true,
                remote: {
                    url: '/user/email/exist',
                    type: 'post',
                    data: {
                        email: function () {
                            return $form.find('input#email').val();
                        }
                    }
                }
            }
        },
        messages : {
            email : {
                remote : 'this is not existed email'
            }
        },
        submitHandler : function(form) {
            var $form = $(form);

            if(!$form.valid()) return;

            $form.ajaxSubmit({
                beforeSubmit : function() {
                    $.mobile.loading('show');
                },
                success : function(response, statusText, jqxhr) {
                    var loc = jqxhr.getResponseHeader('Location');

                    $.mobile.loading('show',{textonly:true, textVisible:true, text:'Authentication mail is sented! Checkout your email account.'})
                    setTimeout(function(){
                        $.mobile.loading('hide');
                        if(loc) $.mobile.navigate(loc);
                    },3000);
                },
                error : function (jqxhr) {
                    $.mobile.loading('hide');

                    switch(jqxhr.status) {
                        case 404 :
                            var v = $form.validate();
                            v.showErrors({email:'email is not exist!'});
                            break;
                    }
                }
            });
        }
    });
});