/**
 * Created by leedong-ho on 2014. 6. 8..
 */
function startQuiz(elem, just) {
    var $btn = $(elem),
        id = $btn.attr('data-quiz-id');

    $.mobile.loading('show', {textVisible:false});

    if(just) {
        $btn.fadeOut(function(){
            $('#start-quiz-page .end-btn').fadeIn();
        });
        return;
    }

    $.ajax({
        type : 'put',
        url : '/quiz/start/' + id,
        success : function() {
            $btn.fadeOut(function(){
                $('#start-quiz-page .end-btn').fadeIn();
            });
        }
    });
};
function endQuiz(elem) {
    var $btn = $(elem),
        id = $btn.attr('data-quiz-id');

    $.ajax({
        type : 'put',
        url : '/quiz/end/' + id,
        success : function() {
            $btn.fadeOut(function(){
                $('#start-quiz-page .start-btn').fadeIn();
                $.mobile.loading('hide');
            });
        },
        error : function(jqxhr) {
            $.mobile.navigate('/user/login');
        }
    });
}
$('#start-quiz-page').on('pagebeforeshow',function(){

});