/**
 * Created by leedong-ho on 2014. 6. 8..
 */
$('#entrance-page').on('pagecreate',function() {
    var $entranceForm = $('#entrance-form'),
        lengthMsg = "Please enter room number that length is {0}!";

    $entranceForm.validate({
        submitHandler : function (form) {
            var $form = $(form);

            if(!$form.valid()) return;

            $form.ajaxSubmit({
                success : function(response, statusText, jqxhr, $form) {
                    $.mobile.navigate(jqxhr.getResponseHeader('Location') + '?' + $.now());
                },
                error : function (jqxhr, statusText, errorThrown, $form) {
                    console.log(jqxhr.status);
                }
            });
        },
        rules : {
            room_idx : {
                minlength : 9,
                maxlength : 10,
                required : true,
                number : true,
                remote : {
                    url : '/user/room_idx/exist',
                    type : 'post',
                    data : {
                        room_idx : function(){
                            return $entranceForm.find('#room_idx').val();
                        }
                    }
                }
            }
        },
        messages : {
            room_idx : {
            minlength : lengthMsg,
            maxlength : lengthMsg,
            remote : 'this room number is not exist!'
          }
        }
    });
});
