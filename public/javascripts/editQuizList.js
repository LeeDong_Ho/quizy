/**
 * Created by leedong-ho on 2014. 6. 25..
 */
function clickRemoveModeBtn(elem) {
    var $btn = $(elem),
        $removeQuizList = $('#remove-quiz-list'),
        $editQuizList = $('#edit-quiz-list'),
        themeBtnB = 'ui-btn-b',
        iconCheck = 'ui-icon-check',
        iconMinus = 'ui-icon-minus';

    if($btn.hasClass(themeBtnB)) {
        $btn.removeClass(themeBtnB).removeClass(iconCheck).addClass(iconMinus);

        $removeQuizList.slideUp(function(){
            $editQuizList.slideDown();
        });
    } else {
        $btn.addClass(themeBtnB).removeClass(iconMinus).addClass(iconCheck);

        $editQuizList.slideUp(function(){
            $removeQuizList.slideDown();
        });
    }
}
function removeQuiz(elem) {
    var $btn = $(elem),
        $popup = $btn.closest('[data-role=popup]'),
        $a = $popup.data('cur'),
        $removeListview = $a.closest('[data-role=listview]');
        id = $a.attr('data-quiz-id'),
        url = '/quiz/' + id;

    if(!id) return;

    $.ajax({
        url:url,
        type:'delete',
        beforeSend : function() {
            $.mobile.loading('show');
        },
        success : function () {
            var $li = $a.closest('li'),
                $editLi = $('#edit-quiz-list').find('a[data-quiz-id='+id+']').closest('li');

            $popup.popup('close');

            $li.fadeOut(function(){
                var $editListview = $editLi.closest('[data-role=listview]');

                $li.remove();
                $removeListview.listview('refresh');
                $editLi.remove();
                $editListview.listview('refresh');
            });
        },
        complete : function() {
            $.mobile.loading('hide');
        }
    });
}
$('#edit-quiz-list-page').on('pagecreate', function() {
    console.log('edit quiz created!');

    var $editAs = $('ul#edit-quiz-list').find('a'),
        $removeAs = $('ul#remove-quiz-list').find('a'),
        editAsLength = $editAs.length;

    for(var i = 0;i<editAsLength;++i) {
        var $a = $editAs.eq(i),
            origin = $a.attr('data-origin-href');

        if(!origin || origin == '') {
            origin = $a.attr('href');
            $a.attr('data-origin-href', origin);
        }

        $a.attr('href', origin + '?' + $.now());
    }

    $removeAs.click(function(e){
        $('#remove-quiz-popup').data('cur', $(e.target));
    });
});