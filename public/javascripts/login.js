/**
 * Created by leedong-ho on 2014. 5. 27..
 */
$('#login-page').page({'create' : function(){
    var $loginForm = $('#login-form');

    $loginForm.validate({
        rules : {
            email : {
                required : true,
                email : true,
                remote : {
                    url : '/user/email/exist',
                    type : 'post',
                    data : {
                        email : function() {
                            return $loginForm.find('input#email').val();
                        }
                    }
                }
            },
            pass : {
                required : true,
                minlength : 6
            }
        },
        messages : {
            email : {
                remote : 'this is not existed email'
            }
        },
        submitHandler : function (form) {
            var $form = $(form);

            if(!$form.valid()) return;

            $form.ajaxSubmit({
                beforeSubmit : function () {
                    $.mobile.loading('show');
                },
                success : function(response, statusText, jqxhr, $form) {
                    var loc = jqxhr.getResponseHeader('Location');
                    $.mobile.navigate(loc);
                },
                error : function(jqxhr, statusText, errorThrown, $form) {
                    switch (jqxhr.status) {
                        case 404 :
                            var v = $form.validate();
                            v.showErrors({pass:'password is not correct!'});
                            break;
                        case 401 :
                            var v = $form.validate();
                            v.showErrors({email:'email is not authenticated!'});
                            break;
                    }
                },
                complete : function () {
                    $.mobile.loading('hide');
                }
            });
        }
    });
}});