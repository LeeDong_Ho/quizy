/**
 * Created by leedong-ho on 2014. 6. 28..
 */
function reportQuiz(elem) {
    var $btn = $(elem),
        url = $btn.attr('data-url');

    $.ajax({
        url:url,
        timeout:15000,
        beforeSend : function () {
            $.mobile.loading('show', {textVisible:true, text:'Sending...'});
        },
        success : function () {
            $.mobile.loading('show', {textonly:true, textVisible:true, text:'Success! Check out your email.'});
            setTimeout(function(){
                $.mobile.loading('hide');
            },5000);
        },
        error : function () {
            $.mobile.loading('show', {textonly:true, textVisible:true, text:'Error Occured!'});
            setTimeout(function(){
                $.mobile.loading('hide');
            },5000);
        },
        complete : function () {
            $('#report-quiz-popup').popup('close');
        }
    });


}
$('#res-quiz-list-page').on('pagecreate', function() {

});