/**
 * Created by leedong-ho on 2014. 6. 25..
 */
function logout() {
    $.mobile.loading('show');
    $.ajax({
        url : '/user/logout',
        type :'post',
        success : function(){
            $.mobile.loading('hide');
            $.mobile.navigate('/user/login');
        },
        error : function() {
            $.mobile.loading('show',{textonly:true,text:'Error Occured! please try again.',textVisible:true});
            setTimeout(function(){
                $.mobile.loading('hide');
            },3000);
        }
    });
}
$('#user-menu-page').on('pagecreate',function(){
    var $as = $('#user-menu-page').find('[data-role=listview] li a'),
        asLength = $as.length;

    for(var i = 0;i<asLength;++i) {
        var $a = $as.eq(i),
            origin = $a.attr('data-orgin-href');

        if(!origin || origin == '') {
            origin = $a.attr('href');
            $a.attr('data-origin-href', origin);
        }

        $a.attr('href', origin + '?' + $.now());
    }
});