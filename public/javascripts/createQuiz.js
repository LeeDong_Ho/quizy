/**
 * Created by leedong-ho on 2014. 5. 18..
 */
function addSa()
{
    if(!addSa.count) addSa.count = 0;

    var $sa = $('<div class="qz-ti-wrap qz-sa", data-idx=' + (addSa.count++) + '>' +
            '<input type="hidden" class="change-idx qz-type" name="quiz[:%qi%:][type]" value="sa" />' +
            '<input type="hidden" class="change-idx qz-upld-url" name="quiz[:%qi%:][upload][url]" value="" />' +
            '<input type="hidden" class="change-idx qz-upld-ct-type" name="quiz[:%qi%:][upload][content_type]" value="" />' +
            '<div class="uploaded-data-wrap">' +
                '<div class="inner-wrap"></div>' +
                '<button class="remove-file-btn" type="button" onclick="removeUploadedFile(this)" data-icon="minus">Remove File</button>' +
            '</div>' +
            '<button class="add-file-btn" type="button" onclick="popupToUploadFile(this)" data-icon="plus">Add File</button>' +
            '<input type="text" class="change-idx ques" name="quiz[:%qsa' + addSa.count + '%:][ques]" placeholder="Question" data-rule-required="true">' +
            '<input type="text" class="change-idx ans" name="quiz[:%qsa' + addSa.count + '%:][ans]" placeholder="Your Answer" data-rule-required="true"></textarea>' +
            '<button type="button" data-icon="minus" onclick="removeSa(this)">Remove Short Answer</button>' +
        '</div>');
    $sa.appendTo('#quiz-form').trigger('create');
    //addSaValidRules($sa);
}
function removeSa(elem)
{
    var $btn = $(elem);
    removeUploadedFile(elem);
    $btn.parent().remove();
}
function addMc()
{
    if(!addMc.count) addMc.count = 0;

    var $mc = $('<div class="qz-ti-wrap qz-mc" data-idx="' + (addMc.count++) + '">' +
            '<input type="hidden" class="change-idx qz-type" name="quiz[:%qi%:][type]" value="mc" />' +
            '<input type="hidden" class="change-idx qz-upld-url" name="quiz[:%qi%:][upload][url]" value="" />' +
            '<input type="hidden" class="change-idx qz-upld-ct-type" name="quiz[:%qi%:][upload][content_type]" value="" />' +
            '<div class="uploaded-data-wrap">' +
                '<div class="inner-wrap"></div>' +
                '<button class="remove-file-btn" type="button" onclick="removeUploadedFile(this)" data-icon="minus">Remove File</button>' +
            '</div>' +
            '<button class="add-file-btn" type="button" onclick="popupToUploadFile(this)" data-icon="plus">Add File</button>' +
            '<input type="text" class="change-idx ques" name="quiz[:%qmc' + (addMc.count++) + '%:][ques]" placeholder="Question" data-rule-required="true"/>' +
            '<fieldset data-role="controlgroup">' +
            '<div class="error-placement"></div>' +
            '</fieldset>' +
            '<button class="add-ques" type="button" onclick="addMcQues(this)" data-icon="plus">Add Question</button>' +
            '<button type="button" onclick="removeMc(this)" data-icon="minus">Remove Multiple Choice</button>' +
        '</div>');
    $mc.appendTo('#quiz-form').trigger('create');
    //addMcValidRules($mc);
    addMcQues($mc.find('.add-ques'));
}
function removeMc(elem)
{
    removeUploadedFile(elem);
    $(elem).closest('div').remove();
}
function addMcQues(elem)
{
    var $mcWrap = $(elem).closest('.qz-mc'),
        $contrgrp = $mcWrap.find('.ui-controlgroup'),
        $cbWrap = $contrgrp.find('.ui-controlgroup-controls'),
        $label = $('<label for="">New Question</label>'),
        $cb = $('<input type="checkbox" class="change-idx mc-cb" name="quiz[:%qmc' + $mcWrap.attr('data-idx') + '%:][cb][]" value=""/>'),
        $hidden = $('<input type="hidden" class="change-idx mc-item" name="quiz[:%qi%:][item][]" value=""/>');

    $cb.click({label:$label,cb:$cb,hidden:$hidden},popupToEditMcQues);

    $cbWrap.append($label.append($cb));
    $cb.checkboxradio({
        create : function(e){
            $(e.target).after($hidden);
        }
    });
    $cb.rules( 'add', {
        required:true,
        minlength:1,
        messages : {
            required : 'you must choose one or more'
        }
    });
    $contrgrp.trigger('create').controlgroup('refresh');
}
function popupToEditMcQues(e)
{
    var $cb = $(e.data.cb),
        $hidden = $(e.data.hidden),
        $popup = $('#edit-mc-ques'),
        $newItem = $popup.find('input[name=item]'),
        $newCb = $popup.find(':checkbox'),
        $contrgrp  = $cb.closest('fieldset[data-role=controlgroup]'),
        $removeBtn = $popup.find('button.remove-ques');

    if($contrgrp.find(":checkbox").length == 1) $removeBtn.hide();
    else $removeBtn.show();

    $newItem.val($hidden.val());
    $newCb.prop('checked', $cb.prop('checked')).checkboxradio('refresh');
    $cb.valid();
    $popup.popup('open');
    $popup.find('button').data('cur', e.data).data('new',{cb:$newCb,item:$newItem});
    $popup.find('input[name=item]').get(0).focus();
}
function popupToUploadFile(elem)
{
    var $wrap = $(elem).closest('div');
    var $popup = $('#upload-file');

    $popup.data('cur', $wrap);
    setProgressBar(0);
    $popup.popup('open');
}
function saveQues(elem)
{
    var $btn = $(elem),
        $form = $btn.closest('form'),
        $popup = $btn.closest('.ui-popup'),
        curData = $btn.data('cur'),
        newData = $btn.data('new');

    if(!$form.valid()) return;

    curData.cb.prop('checked',newData.cb.prop('checked')).checkboxradio('refresh');
    curData.label.text(newData.item.val());
    curData.hidden.val(newData.item.val());
    $popup.popup('close');
    curData.cb.valid();
}
function removeQues(elem)
{
    var $btn = $(elem),
        curData = $btn.data('cur'),
        $contrGrp = curData.cb.closest('.ui-controlgroup'),
        qzOffset = $contrGrp.closest().offset('.qz-ti-wrap'),
        $popup = $btn.closest('.ui-popup');

    curData.cb.closest('.ui-checkbox').remove();
    $contrGrp.controlgroup('refresh');
    $popup.popup('close');
    $('html, body').animate({scrollTop:qzOffset.top});
}
function postIdxWithTestItem(form)
{
    var $form = $(form),
        $items = $form.find('.qz-ti-wrap'),
        itemLength = $items.length,
        regForQuizIdx = /\:\%q[0-9a-z]+\%\:/;

    for(var i = 0;i < itemLength;++i) {
        var $item = $items.eq(i),
            $qType = $item.find('.qz-type'),
            $changeIdxs = $item.find('.change-idx'),
            changeIdxslength = $changeIdxs.length;

        for(var j = 0;j < changeIdxslength;++j) {
            var $changeElem = $changeIdxs.eq(j),
                changeElemName = $changeElem.attr('name');

            $changeElem.attr('name', changeElemName.replace(regForQuizIdx, i));
        }

        if($qType.val() && $qType.val() != 'mc') continue;

        var $cbs = $item.find('.mc-cb:checkbox'),
            $inputItems = $item.find('.mc-item'),
            cbsLength = $cbs.length;

        for(var count = 0, k = 0;k <cbsLength;++k) {
            if($inputItems.eq(k).val()) {
                $cbs.eq(k).val(count++);
            }
        }
    }
}
function removeUploadedFile(elem) {
    var $btn = $(elem),
        $qzWrap = $btn.closest('.qz-ti-wrap'),
        $uploadedDatawrap = $btn.parent('div'),
        $innerWrap = $uploadedDatawrap.find('.inner-wrap'),
        $media = $innerWrap.find('img, video, audio, a'),
        $hiddenUrl = $qzWrap.find('input.qz-upld-url'),
        $hiddenCtType = $qzWrap.find('input.qz-upld-ct-type'),
        url;

    if($media.is('img')) url = $media.attr('src');
    else if ($media.is('video') || $media.is('audio')) url = $media.find('source').attr('src');
    else url = $media.attr('href');
    //video, audio

    if(!url) return;

    $.ajax({
        url : url,
        type : 'delete',
        success : function () {
            $innerWrap.find('*').remove();
            $btn.fadeOut();
            $btn.closest('.qz-ti-wrap').find('.add-file-btn').fadeIn();
            $hiddenCtType.val('');
            $hiddenUrl.val('');
        },
        error : function (jqxhr) {
            handleAjaxError(jqxhr);
        }
    });
}
function handleAjaxError(jqxhr) {
    switch (jqxhr.status) {
        case 401 :
            jQuery.mobile.navigate('/user/login');
            break;
    }
}
function addMcValidRules($mc) {
    var $ques = $mc.find('input.ques');

    $ques.attr('id', "ques-" + $mc.attr('data-idx'));
    $ques.rules('add', {required:true});
}
function addSaValidRules($sa) {
    var $ques = $sa.find('input.ques'),
        $ans = $sa.find('input.ans');

    $ques.attr('id', 'sa-ques-' + $sa.attr('data-idx')).rules('add', {required:true});
    $ans.attr('id', 'sa-ans-' + $sa.attr('data-idx')).rules('add', {required:true});
}
function setProgressBar(val) {
    $('#progress-bar').val(val).slider('refresh');
}
$('.quiz-page').on('pagecreate',function(){
    console.log('quiz page created!!')

    var $qzForm = $('#quiz-form'),
        $editMcQuesForm = $('#edit-mc-ques');

    $qzForm.validate({
        errorPlacement : function(err, elem) {
            if(elem.is(':checkbox')){
                elem.closest('[data-role=controlgroup]').find('.error-placement').append(err);
            } else {
                elem.after(err);
            }
        },
        submitHandler : function(form) {
            var $form = $(form);

            if(!$form.valid()) return;

            postIdxWithTestItem(form);
            $(form).ajaxSubmit({
                beforeSubmit : function() {
                    $.mobile.loading('show');
                },
                success : function (response, statusText, jqxhr) {
                    var loc = jqxhr.getResponseHeader('Location');
                    if(loc) $.mobile.navigate(loc);
                },
                complete : function() {
                    $.mobile.loading('hide');
                }
            });
        }
    });

    $editMcQuesForm.validate();

    $('.sbmt-btn').click(function(){
        $qzForm.submit();
    });

    $('#upload-form').ajaxForm({
        beforeSubmit : function(arr, $form) {
            if( $form.valid() ) $.mobile.loading('show',{textvisible:true, text:'uploading..'});
        },
        success : function (response, statusText, jqxhr) {
            var contentType = '',
                fileName = '',
                src = jqxhr.getResponseHeader('Location'),
                $popup = $('#upload-file'),
                $wrap = $popup.data('cur'),
                $hiddenUrl = $wrap.find('input.qz-upld-url'),
                $hiddenCtType = $wrap.find('input.qz-upld-ct-type'),
                $bowl;

            if(response && response.contentType) contentType = response.contentType;
            else $bowl = "<div>no contetn type!</div>";

            if(response && response.fileName) fileName = response.fileName;

            $hiddenCtType.val(contentType);
            $hiddenUrl.val(src);

            if(contentType.search('image/') != -1) {
                $bowl = $('<img />').attr('src', src);
            } else if (contentType.search('video/') != -1) {
                var $source = $('<source />');

                $bowl = $('<video controls>This browser do not support HTML5 video!!</video>');
                $source.attr('src', src).attr('type', contentType);
                $bowl.append($source);
            } else if (contentType.search('audio/') != -1) {
                var $source = $('<source />');

                $bowl = $('<audio controls>This browser do not support HTML5 audio</audio>');
                $source.attr('src', src).attr('type', contentType);
                $bowl.append($source);
            } else {
                $bowl = $('<a></a>');
                $bowl.attr('href', src).text(fileName);
            }

            $wrap.find('.uploaded-data-wrap .inner-wrap').append($bowl);

            $wrap.find('.add-file-btn').fadeOut(function(){
                $wrap.find('.remove-file-btn').fadeIn();
            });

            $wrap.find('input.ques').focus();
            $.mobile.loading('hide');
            $popup.popup('close');
        },
        error : function(jqxhr) {
            $.mobile.loading('hide');
            handleAjaxError(jqxhr);
        },
        uploadProgress : function (e, pos, total, percentComplete)
        {
            setProgressBar(percentComplete);
        }
    });

    $('<input>').appendTo('#upload-form #progress-bar-wrap').attr({'name':'progress_bar','id':'progress-bar','data-highlight':'true','min':'0','max':'100','value':'50','type':'range'}).slider({
        create: function( event, ui ) {
            $(this).parent().find('input').hide();
            $(this).parent().find('input').css('margin-left','-9999px'); // Fix for some FF versions
            $(this).parent().find('.ui-slider-track').css('margin','0 15px 0 15px');
            $(this).parent().find('.ui-slider-handle').hide();
        }
    }).slider("refresh");
});