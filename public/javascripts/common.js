/**
 * Created by leedong-ho on 2014. 6. 9..
 */
function playVideo(elem) {
    var video = $(elem).closest('video').get(0);

    if(video.paused) video.play();
    else video.pause();
}