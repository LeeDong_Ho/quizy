/**
 * Created by leedong-ho on 2014. 5. 27..
 */
$('#signup-page').on('pagebeforeshow',function(){
    var $signupForm = $('#signup-form');

    $signupForm.validate({
        rules : {
            email : {
                required : true,
                email : true,
                remote : {
                    url : '/user/email/not_exist',
                    type : 'post',
                    data : {
                        email : function() {
                            return $signupForm.find('input#email').val();
                        }
                    }
                }
            },
            pass : {
                required : true,
                minlength : 8
            },
            pass_again : {
                required : true,
                minlength : 8,
                equalTo:'#signup-form #pass'
            }
        },
        messages : {
            email : {
                remote : 'this email is existed'
            }
        },
        submitHandler : function (form) {
            var $form = $(form);

            if(!$form.valid()) return;

            $form.ajaxSubmit({
                beforeSubmit : function() {
                    $.mobile.loading('show');
                },
                success : function(response, statusText, jqxhr) {
                    var loc = jqxhr.getResponseHeader('Location');

                    $.mobile.navigate(loc);
                },
                error : function(err, jqxhr, errorThrown) {
                    //error handling
                },
                complete : function() {
                    $.mobile.loading('hide');
                }
            });
        }
    });
});