exports.schema = {
    email : { type : String, required : true },
    email_auth : {type : Boolean, default:'false'},
    pass : { type : String, required : true },
    room_idx : { type : Number },
    reg_date : { type : Date, default : Date.now }
}