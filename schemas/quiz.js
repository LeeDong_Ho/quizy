var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

exports.schema = {
    user_id : { type : ObjectId, required : true },
    reg_date : { type : Date, default : Date.now },
    mod_date : { type : Date, default : Date.now },
    quiz_sbjt : String,
    quiz: Object,
    start : Boolean
}