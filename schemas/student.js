/**
 * Created by leedong-ho on 2014. 6. 9..
 */
var mongoose = require('mongoose');
var ObjectId = mongoose.Schema.Types.ObjectId;

exports.schema = {
    quiz_id : { type : ObjectId, required : true },
    quiz_res : Object,
    reg_date : { type : Date, default : Date.now },
    mod_date : { type : Date, default : Date.now }
}