var util = require('util');
var Contr = require('./controller');
var model = require('../models/quiz').getInstance();
var instance;
var QuizContr = function quizContr( model ) {
    QuizContr.super_.call(this, model);
    this.create = function (req, res) {
        var newQuizDoc,
            context = this,
            async = require('async');

        newQuizDoc = this.model.getNewDoc();
        newQuizDoc.user_id = req.session.user ? req.session.user._id : null;
        newQuizDoc.quiz = req.body;
        newQuizDoc.save(function(err) {
            if(err) return context.handleError(err, res);
            res.status(301).set('Location','/user').send();
        });
    };

    this.saveUploadedFile = function (req, res) {
        var multiparty = require('multiparty'),
            form = new multiparty.Form(),
            async = require('async'),
            conf = require('../conf'),
            context = this;

            try {
                async.waterfall([
                    function parseBody(callback) {
                        form.parse(req, function parsedBody(err, field, files) {
                            callback(err, field, files);
                        });
                    },
                    function isUploaded(field, files, callback) {
                        var mongoose = context.mongoose,
                            conn = mongoose.createConnection(conf.uploading.url),
                            file = (files.f && files.f.length > 0) ? files.f.pop() : null;

                        if(!file) {
                            res.status(500).send();
                            return;
                        }

                        conn.once('open', function() {
                            callback(null, file, conn, mongoose);
                        });
                    },
                    function saveFile(file,  conn, mongoose, callback) {
                        var user = req.session.user,
                            Grid = require('gridfs-stream');

                        if (!user) {
                            var err = new Error();
                            err.code = 401;
                            return callback(err);
                        }

                        var gfs = Grid(conn.db, mongoose.mongo),
                            newObjId = new mongoose.Types.ObjectId(),
                            option = {
                                _id : newObjId.toString(),
                                filename : file.originalFilename,
                                size : file.size,
                                metadata : {
                                    user_id : user._id.toString(),
                                    contentType : file.headers['content-type']
                                }
                            },
                            writeStream = gfs.createWriteStream(option),
                            fs = require('fs');

                        fs.createReadStream(file.path).pipe(writeStream);

                        writeStream.on('close', function(file){
                            callback(null, file, newObjId);
                        });
                    },
                    function closedFile(file, newObjId) {
                        res.status(201).set('Location','/quiz/uploaded/' + newObjId.toString()).json({'contentType':file.metadata.contentType});
                    }
                ],
                function(err, result) {
                   if(err) context.handleError(err, res);
                });
            } catch (err) {
                this.handleError(err, res);
            };
    };

    this.readUploadedFile = function(req, res) {
        var async = require('async'),
            context = this,
            conf = require('../conf');

        try {
            async.waterfall([
                function openConn(callback) {
                    var mongoose = context.mongoose,
                        conn = mongoose.createConnection(conf.uploading.url);

                    conn.once('open', function() {
                        callback(null, mongoose, conn);
                    });
                },
                function readMetaData (mongooose, conn, callback) {
                    if(!req.params || !req.params.id) {
                        var err = new Error('no file id');
                        err.code = 404;
                        callback(err);
                    }

                    var Grid = require('gridfs-stream'),
                        gfs = Grid(conn.db, mongooose.mongo),
                        objId = new mongooose.Types.ObjectId(req.params.id);

                    gfs.files.findOne({ _id : objId }, function(err, file) {
                        callback(err, file, gfs, mongooose, objId);
                    });
                },
                function readFile(file, gfs, mongoose, objId, callback) {
                    if(!file)
                    {
                        var err = new Error();
                        err.code = 404;
                        return callback(err);
                    }

                    if(req.headers.range) {
                        var rangeParser = require('range-parser'),
                            parsedRangeHeader = rangeParser(file.length, req.headers.range),
                            range = parsedRangeHeader[0],
                            status = 206,
                            chunksize;

                        if(range < 0) return res.status(416).send();

                        chunksize = range.end - range.start + 1;

                        res.writeHead(status, {
                            'Content-Range': 'bytes ' + range.start + '-' + range.end + '/' + file.length,
                            'Accept-Ranges': 'bytes',
                            'Content-Length': chunksize,
                            'Content-Type': file.metadata.contentType
                        });

                        console.log(res._headers);
                        console.log(req.headers);

                        readStream = gfs.createReadStream({
                            _id : objId,
                            range : {
                                startPos : range.start,
                                endPos : range.end
                            }
                        }).pipe(res);

                        readStream.once('error', function (err) {
                            if(err.message && err.message.search('does not exist') != -1) err.code = 404;
                            callback(err);
                        });

                        return;
                    }

                    console.log(req.headers);

                    var readStream,
                        expires;

                    if(req.get('If-Modified-Since')) return res.status(304).send();

                    res.set('Content-Type', file.metadata.contentType);

                    if(file.metadata.contentType.search('video/') != -1) return res.send();

                    expires = new Date();
                    expires.setFullYear(expires.getFullYear() + 2);

                    res.set('Cache-Control','public, max-age=' + conf.uploading.maxAge);
                    res.set('Last-Modified', file.uploadDate.toString());
                    res.set('Expires', expires.toString());

                    readStream = gfs.createReadStream({
                        _id : objId
                    }).pipe(res);

                    readStream.once('error', function (err) {
                        if(err.message && err.message.search('does not exist') != -1) err.code = 404;
                        callback(err);
                    });
                }
            ],
            function (err, result) {
                if(err) return context.handleError(err, res);
            });
        } catch (err) {
            if(err) this.handleError(err, res);
        }
    };

    this.removeUploadData = function (req, res)
    {
        var async = require('async'),
            conf = require('../conf'),
            context = this;

        try {
            async.waterfall([
                function openConn(callback) {
                    var mongoose = context.mongoose,
                        conn = mongoose.createConnection(conf.uploading.url);

                    conn.once('open', function() {
                        callback(null, mongoose, conn);
                    });
                },
                function readMetaData(mongoose, conn, callback) {
                    var Grid = require('gridfs-stream'),
                        gfs = Grid(conn.db, mongoose.mongo),
                        objId;

                   if(!req.params.id) {
                       var err = new Error();
                       err.code = 404;
                       return callback(err);
                   }

                   objId = new mongoose.Types.ObjectId(req.params.id);

                   gfs.files.findOne({ _id : objId },function(err, file) {
                        if(!err && !file) return res.status(404).send();
                        callback(err, file, gfs);
                   });
                },
                function removeFile(file, gfs, callback) {
                    var user = req.session.user;

                    if(!user) return callback(Error('Unauthorized'));

                    gfs.remove({_id:file._id}, function(err){
                       callback(err);
                    });
                },
                function removed() {
                    res.status(204).send();
                }
            ], function (err) {
                context.handleError(err, res);
            });
        } catch (err) {

        }
    };
};
util.inherits(QuizContr,Contr);
exports.getInstance = function() {
    if(!instance) instance = new QuizContr(model);
    return instance;
};