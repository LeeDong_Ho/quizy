module.exports = function Model (collName) {
    this.coll;
    this.collName = collName;//왜 이걸로 collection을 생성하는데 quizzes로 변경되서 만들어지지?
    this.mongoose = require('mongoose');
    this.schema = require('../schemas/' + collName).schema;
    this.coll = this.mongoose.model(this.collName, new this.mongoose.Schema(this.schema));

    this.getNewDoc = function()
    {
        //collection의 doc 인스턴스
        return new this.coll();
    };
};