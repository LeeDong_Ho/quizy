var instance;
var util = require('util');
var Model = require('./model');
var QuizModel = function quizModel (collName) {
    QuizModel.super_.call(this, collName);
    var conf = require('../conf');
    this.uploadingConn = this.mongoose.createConnection(conf.uploading.url);

    this.getQuizzesByQry = function (qry, callback) {
        var async = require('async'),
            context = this;

        try {
            async.waterfall([
                function findQuizzes(callback) {
                    context.coll.find(qry, function(err, quizzes) {
                        callback(err, quizzes);
                    });
                },
                function returnRes(quizzes) {
                    callback(null, quizzes);
                }
            ], function(err) {
                callback(err);
            });
        } catch (err) {
            callback(err);
        }
    };

    this.getQuizzes = function (userObjId, callback) {
        var async = require('async'),
            context = this,
            result;

        try {
            async.waterfall([
               function findQuizzes(callback) {
                   context.coll.find({user_id:userObjId}, function(err, quizzes) {
                        callback(err, quizzes);
                   });
               },
               function returnRes(quizzes) {
                    callback(null, quizzes);
               }
            ], function(err) {
                callback(err);
            });
        } catch (err) {
            callback(err);
        }
    }

    this.getQuiz = function (quizObjId, callback) {
        var async = require('async'),
            context = this;
        try {
            async.waterfall([
                function getQuizByObjId(callback) {
                    context.coll.findOne({ _id : quizObjId.toString() }, function (err, quiz) {
                        callback(err, quiz);
                    });
                },
                function returnQuiz(quiz) {
                    callback(null, quiz);
                }
            ], function (err) {
                callback(err);
            });
        } catch (err) {
            callback(err);
        };
    };

    this.getQuizOne = function (qry, callback) {
        var async = require('async'),
            context = this;
        try {
            async.waterfall([
                function findQuiz(callback) {
                    context.coll.findOne(qry, function (err, quiz) {
                        callback(err, quiz);
                    });
                },
                function returnQuiz(quiz) {
                    callback(null, quiz);
                }
            ], function (err) {
                callback(err);
            });
        } catch (err) {
            callback(err);
        };
    };
};
util.inherits(QuizModel,Model);
exports.getInstance = function() {
    if(!instance) instance = new QuizModel('quiz');
    return instance;
}