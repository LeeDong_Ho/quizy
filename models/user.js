var instance;
var util = require('util');
var Model = require('./model');
var UserModel = function userModel (collName) {
    UserModel.super_.call(this, collName);

    this.findUser = function(qry, callback) {
        var async = require('async'),
            context = this;

        try {
            async.waterfall([
                function findUserByQry(callback) {
                    context.coll.findOne(qry, function(err, user){
                        callback(err, user);
                    });
                },
                function result(user) {
                    callback(null, user);
                }
            ], function(err) {
                if(err) callback(err);
            });
        } catch (err) {
            callback(err);
        }
    };

};
util.inherits(UserModel, Model);
exports.getInstance = function() {
    if(!instance) instance = new UserModel('user');
    return instance;
}