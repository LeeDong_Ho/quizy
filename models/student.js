/**
 * Created by leedong-ho on 2014. 6. 9..
 */
var instance;
var util = require('util');
var Model = require('./model');
var StudentModel = function StudentModel (collName) {
    StudentModel.super_.call(this, collName);

    this.findQuizzes = function(qry, callback){
        this.coll.find(qry, function(err, quizzes) {
            callback(err, quizzes);
        });
    }
};
util.inherits(StudentModel, Model);
exports.getInstance = function() {
    if(!instance) instance = new StudentModel('student');
    return instance;
}