module.exports = function(app) {
    app.get('/', function(req, res) {
        var conf = require('../conf');
        //강의자인지 학생인지 선택할 수 있는 홈페이지
        res.render('index',conf.host);
    });
    require('./quiz')(app);
    require('./user')(app);
    require('./student')(app);
}