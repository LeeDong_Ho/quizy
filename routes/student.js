/**
 * Created by leedong-ho on 2014. 6. 8..
 */
module.exports = function(app) {
    var rootPath = '/student',
        quizContr = require('../controllers/quiz').getInstance(),
        userContr = require('../controllers/user').getInstance(),
        studentContr = require('../controllers/student').getInstance();

    //학생 방번호 입력 페이지
    app.get(rootPath, function(req, res) {
        res.render('entrance');
    });

    app.get(rootPath + '/quiz/waiting/:room_idx', function(req, res) {
        if(!req.params.room_idx) return res.status(404).send();
        res.render('waitingQuiz',{room_idx:req.params.room_idx});
    });

    //퀴즈가 있다면 퀴즈를 렌더하고 없다면 404
    app.head(rootPath + '/quiz/exist/:room_idx', function(req, res) {
        var context = {
                quizContr : quizContr,
                rootPath : rootPath,
                req : req,
                res : res
            };
        quizContr.getStartedQuiz(req.params.room_idx, function(err, quiz) {
            if(err) return context.quizContr.handleError(err);

            if(quiz) context.res.status(204).set('Location', context.rootPath +'/quiz/' + req.params.room_idx).send();
            else context.res.status(404).send();
        });
    });

    app.get(rootPath + '/quiz/:room_idx', function(req, res){
        var context = {
            quizContr : quizContr,
            rootPath : rootPath,
            req : req,
            res : res
        };
        quizContr.getStartedQuiz(req.params.room_idx, function(err, quiz) {
            if(err) return context.quizContr.handleError(err, res);

            if(quiz) context.res.render('solveQuiz', {quiz_set:quiz});
            else context.res.render('waitingQuiz', {room_idx:context.req.params.room_idx});
        });
    });

    //퀴즈본 사람 저장 및 채점
    app.post(rootPath + '/quiz/submit/:quiz_id', function(req, res) {
        studentContr.saveQuizRes(req, res);
    });

    //퀴즈 보는 페이지 시작한 퀴즈가 없으면 기다리는 페이지 아니면 퀴즈 푸는 페이지
    app.post(rootPath + '/quiz', function(req, res){
        var context = {
            quizContr : quizContr,
            rootPath : rootPath,
            req : req,
            res : res
        };
        quizContr.getStartedQuiz(req.body.room_idx, function(err, quiz) {
            if(err) return context.quizContr.handleError(err, context.res);

            var loc;

            if(quiz) loc = context.rootPath +'/quiz/' + context.req.body.room_idx;
            else loc = context.rootPath +'/quiz/waiting/' + context.req.body.room_idx;

            context.res.status(200).set('Location', loc).send();
        });
    });
}