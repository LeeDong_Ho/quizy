module.exports = function(app){
    var rootPath = '/user';
    var userContr = require('../controllers/user').getInstance();

    app.get(rootPath, function(req, res) {
        userContr.mustLogin(req, res);
        res.render('userMenu');
    });

    app.get(rootPath + '/login', function(req, res) {
        res.render('login');
    });

    app.get(rootPath + '/signup', function(req, res) {
       res.render('signup');
    });

    app.get(rootPath + '/auth/email', function(req, res) {
        res.render('authMail');
    });

    //이메일 인증
    app.get(rootPath + '/auth/:id/:hmac', function(req, res) {
        userContr.authEmail(req, res);
    });

    //인증 이메일 재전송
    app.post(rootPath + '/auth/email', function(req, res) {
        var email = req.body.email,
            async = require('async'),
            userModel = require('../models/user').getInstance(),
            userContr = require('../controllers/user').getInstance();

        if(!email) return res.status(404).send();

        try {
            async.waterfall([
                function findUser(callback) {
                    userModel.coll.findOne({email:email}, function(err, user) {
                        if(err) return userModel.handleError(err, res);

                        callback(err, user);
                    });
                },
                function send(user, callback) {
                    if(!user) {
                        return res.status(404).send();
                    }

                    userContr.sendAuthMail(user, function(err, response) {
                        callback(err);
                    });
                },
                function response() {
                    res.status(204).set('Location','/user/login').send();
                }
            ], function(err) {
                userContr.handleError(err, res);
            });
        } catch (err) {
           userContr.handleError(err, res);
        }
    });

    app.post(rootPath + '/new', function(req, res) {
        userContr.create(req, res);
    });

    app.post(rootPath + '/login', function(req, res) {
        userContr.login(req, res);
    });

    app.post(rootPath + '/logout', function(req, res) {
        userContr.logout(req, res);
    });

    app.post(rootPath + '/email/exist', function(req, res) {
        userContr.isExistedId(req, res);
    });

    app.post(rootPath + '/email/not_exist', function(req, res) {
        userContr.isNotExistedId(req, res);
    });

    app.post(rootPath + '/room_idx/exist', function(req, res) {
        userContr.isExistedRoomIdx(req, res);
    });
};