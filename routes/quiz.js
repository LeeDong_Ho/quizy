module.exports = function(app) {
    var rootPath = '/quiz',
        quizContr = require('../controllers/quiz').getInstance(),
        userContr = require('../controllers/user').getInstance();

    //test
    app.get('/quiz/test', function(req,res){
        var fs = require('fs');
        fs.exists('public/Kalimba.a', function(exist){
            if(exist)res.attachment('public/Kalimba.a');
            else res.status(404).send();
        })

    });

    //test officegen
    app.get(rootPath + '/xlsx', function(req, res) {
        var xlsx = require('node-xlsx'),
            fs = require('fs');

        var buffer = xlsx.build({worksheets: [
            {"name":"mySheetName", "data":[
                ["A1", "B1",'sum'],
                [
                    {"value":"10","formatCode":"General"},
                    {"value":"20","formatCode":"General"},
                    {"value":"30","formatCode":"General"}
                ],
                [
                    {"value":"30","formatCode":"General"},
                    {"value":"40","formatCode":"General"},
                    {"value":"=SUM(A2:B2)","formatCode":"General"}
                ],
                [
                    {"value":"=SUM(A2:A3)","formatCode":"General"},
                    {"value":"=SUM(B2:B3)","formatCode":"General"},
                    {"value":"=SUM(C2:C3)","formatCode":"General"}
                ]
            ]}
        ]}); // returns a buffer


        res.set('Content-Type','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        res.send(buffer);
    });

    //시작할 목록을 반환
    app.get(rootPath, function(req, res) {
        if(userContr.mustLogin(req, res)) return;

        var async = require('async'),
            user = req.session.user,
            quizModel = require('../models/quiz').getInstance(),
            quizContr = require('../controllers/quiz').getInstance();

        async.waterfall([
            function findStaredQuiz(callback) {
                quizContr.getStartedQuiz(user.room_idx, function(err, quiz){
                    callback(err, quiz);
                });
            },
            function renderIfStaredQuizIsExisted(quiz, callback) {
                if(quiz) return res.render('startQuiz',{user:user, quiz:quiz});

                quizModel.getQuizzes(user._id, function(err, quizzes) {
                    callback(err, quizzes);
                });
            },
            function renderQuizList(quizzes, callback) {
                res.render('startQuizList',{quizzes:quizzes});
            }
        ], function(err){
            if(err) quizContr.handleError(err, res);
        });
    });

    //시험 결과를 가지고 있는 퀴즈의 학생 목록 반환
    app.get(rootPath + '/result/:quiz_id', function(req, res) {
        if(userContr.mustLogin(req, res)) return;

        quizContr.renderQuizResList(req, res);
    });

    //시험 채점 결과
    app.get(rootPath + '/result/student/:student_quiz_id', function(req, res) {
        if(userContr.mustLogin(req, res)) return;

        quizContr.renderEndedStudentQuiz(req, res);
    });

    //시험 결과를 가지고 있는 퀴즈 목록 반환
    app.get(rootPath + '/result', function(req, res) {
        if(userContr.mustLogin(req, res)) return;

        quizContr.renderListEndedQuiz(req, res);
    });

    //퀴즈를 생성하는 마크업 렌더
    app.get(rootPath + '/new', function(req, res) {
        if(userContr.mustLogin(req, res)) return;

        res.render('createQuiz');
    });

    //퀴즈 업로드된 파일 다운로드
    app.get(rootPath + '/uploaded/:id', function(req, res) {
        quizContr.readUploadedFile(req, res);
    });

    //퀴즈 시작 페이지
    app.get(rootPath + '/start/:id', function(req, res) {
        if(userContr.mustLogin(req, res)) return;

        var quizModel = require('../models/quiz').getInstance(),
            quizObjId = quizModel.mongoose.Types.ObjectId(req.params.id),
            async = require('async'),
            user = req.session.user;

        if(!req.params.id) {
            var err = new Error('No quiz id');
            err.code = 401;
            return quizContr.handleError(err, res);
        }

        quizModel.getQuiz(quizObjId, function(err, quiz) {
            if(err) return userContr.handleError(err, res);
            res.render('startQuiz',{user:user, quiz:quiz});
        });
    });

    //퀴즈 파일 업로드
    app.post(rootPath + '/upload', function(req, res) {
        if(userContr.mustLogin(req, res)) return;

        quizContr.saveUploadedFile(req, res);
    });

    //편집할 퀴즈들을 가져옴
    app.get(rootPath + '/edit', function(req, res) {
        if(userContr.mustLogin(req, res)) return;

        var user = req.session.user,
            quizModel = require('../models/quiz').getInstance();

        quizModel.getQuizzesByQry({user_id:user._id}, function(err, quizzes){
            if(err) return quizModel.handleError(err, res);

            res.render('editQuizList', {quizzes:quizzes});
        });
    });

    //퀴즈를 편집하는 마크업 렌더
    app.get(rootPath + '/edit/:id', function(req, res) {
        if(userContr.mustLogin(req, res)) return;

        var quizModel = require('../models/quiz').getInstance();

        quizModel.getQuizOne({_id:req.params.id}, function(err, quiz) {
            if(err) return quizModel.handleError(err, res);

            res.render('editQuiz', {quizSet:quiz});
        });
    });

    //퀴즈 전체 결과 리포트
    app.get(rootPath + '/report/:id', function(req, res) {
        if(userContr.mustLogin(req, res)) return;

        quizContr.reportOfAllQuizRes(req, res);
    });

    //퀴즈 업로드된 파일 퀴즈 문서에서 삭제
    app.put(rootPath + '/uploaded', function(req, res) {
        if(userContr.mustLogin(req, res)) return;

        quizContr.updateUploadedFileFromDoc(req, res);
    });

    //퀴즈 업데이트
    app.put(rootPath + '/:id', function(req, res) {
        if(userContr.mustLogin(req, res)) return;

        quizContr.update(req, res);
    });

    //업로드된 파일 삭제
    app.del(rootPath + '/uploaded/:id', function(req, res) {
        if(userContr.mustLogin(req, res)) return;

        quizContr.removeUploadData(req, res);
    });

    //퀴즈 삭제
    app.del(rootPath + '/:id', function(req, res) {
        if(userContr.mustLogin(req, res)) return;

        quizContr.remove(req, res);
    });

    //퀴즈 생성
    app.post(rootPath + '/new', function(req, res) {
        quizContr.create(req,res);
    });

    //퀴즈 시작
    app.put(rootPath + '/start/:id', function(req, res) {
        userContr.mustLogin(req, res);
        if(!userContr.isLoggedIn(req)) return;

        quizContr.startQuiz(req, res);
    });

    //퀴즈 종료
    app.put(rootPath + '/end/:id', function(req, res) {
        userContr.mustLogin(req, res);
        if(!userContr.isLoggedIn(req)) return;

        quizContr.endQuiz(req, res);
    });
};