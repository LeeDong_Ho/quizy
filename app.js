
/**
 * Module dependencies.
 */
var mongoose = this.mongoose = require('mongoose');
var express = require('express');
var routes = require('./routes');
var http = require('http');
var https = require('https');
var path = require('path');
var conf = require('./conf');
var fs = require('fs');

var app = express();
var cookieParser = require('cookie-parser');
var session = require('express-session');

var key = fs.readFileSync('security/site.key').toString();
var cert = fs.readFileSync('security/final.crt').toString();
var opts = {
    key:key,
    cert:cert
};

// all environments
app.set('port', process.env.PORT || 3000);
app.set('https_port', process.env.PORT || 3443);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(cookieParser());
app.use(session(conf.session));
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

mongoose.connect('mongodb://localhost/' + conf.dbName);

//라우팅 생성
routes(app);

http.createServer(app).listen(app.get('port'), function(){
    console.log('Express server listening on port ' + app.get('port'));
});
https.createServer(opts, app).listen(app.get('https_port'), function(){
    console.log('Express server listening on port ' + app.get('https_port'));
});
